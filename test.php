<?php
require_once "vendor/autoload.php";
require_once "bootstrap.php";
$app = new \Espo\Core\Application();
$app->setupSystemUser();
$entityManager = $app->getContainer()->get('entityManager');
?>