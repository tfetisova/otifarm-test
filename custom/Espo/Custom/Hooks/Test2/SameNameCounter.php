<?php
namespace Espo\Custom\Hooks\Test2;
use Espo\ORM\Entity;

class SameNameCounter extends \Espo\Core\Hooks\Base
{    
    public function beforeSave(Entity $entity, array $options = [])
    {
        $entityManager = $this->getEntityManager();
        $entityName = $entity->get('name');
        $entityList = $entityManager->getRepository('Test2')->where(['name' => $entityName])->find();
        $entity->set('sameNameEntities',count($entityList));
    }
}