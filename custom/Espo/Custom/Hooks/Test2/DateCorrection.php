<?php
namespace Espo\Custom\Hooks\Test2;
use Espo\ORM\Entity;

class DateCorrection extends \Espo\Core\Hooks\Base
{    
    public function beforeSave(Entity $entity, array $options = [])
    {
        if($entity->isFieldchanged('testOptionsABCD') || $entity->isFieldchanged('testPeriod')){
            $multiNum = $entity->get('testOptionsABCD');
            $date = $entity->get('testDate');
            $enum = $entity->get('testPeriod');
            
            
            if (in_array("a", $multiNum) || in_array("b", $multiNum)) {
                switch ($enum) {
                    case "Day":
                        $newDate = date('Y-m-d', strtotime("+1 day", strtotime($date)));
                        break;
                    case "Month":
                        $newDate = date('Y-m-d', strtotime("+1 month", strtotime($date)));
                        break;
                    case "Year":
                        $newDate = date('Y-m-d', strtotime("+1 year", strtotime($date)));
                        break;
                }
            }else{
                switch ($enum) {
                    case "Day":
                        $newDate = date('Y-m-d', strtotime("-1 day", strtotime($date)));
                        break;
                    case "Month":
                        $newDate = date('Y-m-d', strtotime("-1 month", strtotime($date)));
                        break;
                    case "Year":
                        $newDate = date('Y-m-d', strtotime("-1 year", strtotime($date)));
                        break;
                }
            }
            file_put_contents("./data/newDate.txt", $newDate);
            $entity->set('testDate', $newDate);
        }
    }
}
?>