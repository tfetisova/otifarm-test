<?php
namespace Espo\Custom\Hooks\Test2;
use Espo\ORM\Entity;

class AfterDel extends \Espo\Core\Hooks\Base
{    
    public function afterRemove(Entity $entity, array $options = [])
    {
        $counter = $entity->get('counter');
         file_put_contents("./data/counter.txt", $counter);
        $entityName = $entity->get('name');
        $responsible = $entity->get('assignedUser');
        $name = $responsible->get('name');
         file_put_contents("./data/assignedUser.txt", $name);
        $newEntity = $this->getEntityManager()->createEntity("Test2", $new);
        $newEntity->set('counter', $counter);
        $newEntity->set('name', $entityName);
        $newEntity->set('assignedUserId', $responsible->id);
        $newCounter = $newEntity->get('counter');
        $this->getEntityManager()->saveEntity($newEntity);
        file_put_contents("./data/newCounter.txt", $newCounter);
        
    }
}
?>