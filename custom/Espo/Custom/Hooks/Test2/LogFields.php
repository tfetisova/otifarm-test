<?php
namespace Espo\Custom\Hooks\Test2;
use Espo\ORM\Entity;

class LogFields extends \Espo\Core\Hooks\Base
{    
    public function afterSave(Entity $entity, array $options = [])
    {
        $enum = $entity->get('testPeriod');
        
        $date = $entity->get('testDate');
        $multiNum = $entity->get('testOptionsABCD');
        $logResults = implode(",", $multiNum);
        file_put_contents("./data/test.txt", $enum."\n".$date."\n".$logResults);
    }
}
?>