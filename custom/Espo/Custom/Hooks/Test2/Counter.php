<?php
namespace Espo\Custom\Hooks\Test2;
use Espo\ORM\Entity;

class Counter extends \Espo\Core\Hooks\Base
{    
    public function beforeSave(Entity $entity, array $options = [])
    {
        $fieldValue = $entity->get('counter');
        $newValue = ++$fieldValue;
        $entity->set('counter', $newValue);
        
    }
}
